
#include "knapsack.cpp"
#include <any>
#include <string>

bool isHelp(const std::string arg) {
	return arg == "--help" || arg == "-h";
}

void printUsage() {
	std::clog << "Program requieres parameteres in format: \n"
				"<number of items(n)> n*<values items> n*<weights items>\n"
				"All numbers with space between them\n"
				"Then you need to follow given instruction\n";
}

int main(int argc, char* argv[]) {
	//helping
	if (std::any_of(argv, argv+argc, isHelp)) {
		printUsage();
		return 1;
	}
	
	if (argc > 1) {
		int itemCount;
		try {
			itemCount = std::atoi(argv[1]);
			if (itemCount * 2 != argc - 2) {
				std::cerr << "Invalid arguments. Plese see instruction. (--help)\n";
				return 1;
			}
		}
		catch (...) {
			std::cerr << "Invalid arguments. Plese see instruction. (--help)\n";
			return 1;
		}

		int *values = new int[itemCount];
		int *weights = new int[itemCount];

		//getting values
		for (int i = 2; i < 2 + itemCount; i++) {
			try {
				values[i - 2] = std::atoi(argv[i]);
			}
			catch (...) {
				delete[] values;
				delete[] weights;
				std::cerr << "Invalid arguments. Plese see instruction. (--help)\n";
				return 1;
			}
		}

		//getting weights
		for (int i = 2 + itemCount; i < argc; i++) {
			try {
				weights[i - 2 - itemCount] = std::atoi(argv[i]);
			}
			catch (...) {
				delete[] values;
				delete[] weights;
				std::cerr << "Invalid arguments. Plese see instruction. (--help)\n";
				return 1;
			}
		}

		//getting additional info
		int knapsackCap;
		char mode;

		std::cout << "Single-threaded 'S'  or Multi-threaded 'M' ?\n";
		std::cin >> mode;
		std::cout << "Type in knapsack capacity: \n";
		std::cin >> knapsackCap;

		//getting the result
		std::pair<int, std::vector<int>> result;
		auto start = std::chrono::high_resolution_clock::now();
		if (mode == 'S') {
			result = knapSackST(knapsackCap, itemCount, weights, values);
		}
		else if (mode == 'M') {
			result = knapSackMT(knapsackCap, itemCount, weights, values);
		}
		else {
			delete[] values;
			delete[] weights;
			std::cerr << "Invalid arguments. Type 'S' or 'M'\n";
			return 1;
		}
		auto end = std::chrono::high_resolution_clock::now();

		std::cout << "Optimal value is : " << result.first << '\n';

		std::cout << (mode == 'S' ? "SINGLE THREADED" : "MULTI THREADED") << " Time needed: " << to_ms(end - start).count() << " ms\n";
		std::cout << "-------------------\n";

		printItems(std::cout, result.second, weights, values);

		delete[] values;
		delete[] weights;
	}
	else {
		std::cerr << "Invalid arguments. Plese see instruction. (--help)\n";
	}
}



