

#include <random>

namespace {
	double get_random_double() {
		static std::mt19937 mt{ std::random_device{}() };
		static std::uniform_real_distribution<> dist(1, 10);
		return dist(mt);
	}

	void prepareArray(int arr[], int size) {
		for (int i = 0; i < size; i++) {
			arr[i] = get_random_double();
		}
		return;
	}
}