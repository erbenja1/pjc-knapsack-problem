#define CATCH_CONFIG_MAIN
#include "knapsack.cpp"
#include "catch.hpp"
#include "testMethod.cpp"


TEST_CASE("Time check 10 10") {
	int knapCap = 10;
	const int size = 10;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 100 50") {
	int knapCap = 100;
	const int size = 20;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 200 100") {
	int knapCap = 200;
	const int size = 40;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 1000 100") {
	int knapCap = 1000;
	const int size = 80;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 5000 200") {
	int knapCap = 5000;
	const int size = 100;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 10000 300") {
	int knapCap = 10000;
	const int size = 200;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 100000 1000") {
	int knapCap = 100000;
	const int size = 500;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";
}

TEST_CASE("Time check 100000 3000") {
	int knapCap = 100000;
	const int size = 2000;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);

	auto start = std::chrono::high_resolution_clock::now();
	auto resST = knapSackST(knapCap, size, weights, values);

	auto mid = std::chrono::high_resolution_clock::now();

	auto resMT = knapSackMT(knapCap, size, weights, values);
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << '\n';
	std::cout << "capacity: " << knapCap << " -- items: " << size << '\n';
	std::cout << "Single thread - Time needed: " << to_ms(mid - start).count() << " ms\n";
	std::cout << "Multi thread - Time needed: " << to_ms(end - mid).count() << " ms\n";

	std::cout << "\n TimeCHECK DONE \n";
	system("pause");
}