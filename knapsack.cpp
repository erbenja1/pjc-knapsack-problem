
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <mutex>
#include <future>


namespace {
	template <typename TimePoint>
	std::chrono::milliseconds to_ms(TimePoint tp) {
		return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
	}

	std::pair<int, std::vector<int>> knapSackST(int knapsackCap, int itemsCount, int weights[], int values[])
	{
		const int sizeX = itemsCount + 1;
		const int sizeY = knapsackCap + 1;

		//main matrix
		std::vector<int> m(sizeX*sizeY);

		//item iteratations
		for (int i = 0; i < sizeX; i++)
		{
			//weights iterations
			for (int w = 0; w < sizeY; w++)
			{
				if (i == 0 || w == 0) {
					m[(i * sizeY) + w] = 0;
				}
				else {
					if (weights[i - 1] <= w) {
						int a = values[i - 1] + m[(i - 1) * sizeY + (w - weights[i - 1])];
						int b = m[(i - 1) * sizeY + w];
						m[(i * sizeY) + w] = std::max(a, b);
					}
					else {
						m[(i * sizeY) + w] = m[(i - 1) * sizeY + w];
					}
				}
			}
		}

		std::vector<int> packed;
		int currItem = sizeX - 1;
		int currWeight = sizeY - 1;


		//getting packed items
		while (currItem > 0 && currWeight > 0) {
			if (m[currItem * sizeY + currWeight] == m[(currItem - 1) * sizeY + currWeight]) {
				currItem--;
			}
			else {
				packed.push_back(currItem - 1);
				currWeight -= weights[currItem - 1];
				currItem--;
			}
		}

		int result = m[sizeX * sizeY - 1];

		return std::pair<int, std::vector<int>>(result, packed);
	}

	std::pair<int, std::vector<int>> knapSackMT(int knapsackCap, int itemsCount, int weights[], int values[])
	{
		const int sizeX = itemsCount + 1;
		const int sizeY = knapsackCap + 1;

		int tCount = std::thread::hardware_concurrency();
		std::vector<std::thread> threads;

		//main matrix
		std::vector<int> m(sizeX*sizeY);

		//function for threads
		auto f = [&](int i, int tNum) {
			//weights iteration
			for (int w = tNum; w < sizeY; w += tCount)
			{
				if (i == 0 || w == 0) {
					m[(i * sizeY) + w] = 0;
				}
				else {
					if (weights[i - 1] <= w) {
						int a = values[i - 1] + m[(i - 1) * sizeY + (w - weights[i - 1])];
						int b = m[(i - 1) * sizeY + w];
						m[(i * sizeY) + w] = std::max(a, b);
					}
					else {
						m[(i * sizeY) + w] = m[(i - 1) * sizeY + w];
					}
				}
			}
		};


		//item iteratations
		for (int i = 0; i < sizeX; i++)
		{
			//creating threads
			for (int tNum = 0; tNum < tCount; tNum++) {
				threads.push_back(std::thread(f, i, tNum));
			}

			//returning threads
			while (!threads.empty()) {
				threads.back().join();
				threads.pop_back();
			}

		}

		std::vector<int> packed;
		int currItem = sizeX - 1;
		int currWeight = sizeY - 1;


		//getting packed items
		while (currItem > 0 && currWeight > 0) {
			if (m[currItem * sizeY + currWeight] == m[(currItem - 1) * sizeY + currWeight]) {
				currItem--;
			}
			else {
				packed.push_back(currItem - 1);
				currWeight -= weights[currItem - 1];
				currItem--;
			}
		}

		int result = m[sizeX * sizeY - 1];

		return std::pair<int, std::vector<int>>(result, packed);
	}

	std::ostream& printItems(std::ostream &os, std::vector<int> items, int weights[], int values[]) {
		os << "Packed Items\n";
		os << "weight ... value\n";
		while (!items.empty()) {
			os << weights[items.back()] << " ... " << values[items.back()] << "\n";
			items.pop_back();
		}
		return os;
	}
}