
#define CATCH_CONFIG_MAIN
#include "knapsack.cpp"
#include "catch.hpp"
#include "testMethod.cpp"



TEST_CASE("SingleT correct result -- Basic") {
	int knapCap = 10;
	const int size = 5;
	int weights[size] = { 1,2,3,4,5 };
	int values[size] = { 10,20,30,40,60 }; 
	REQUIRE(knapSackST(knapCap, size, weights, values).first == 110);
}

TEST_CASE("SingleT correct result -- Same items") {
	int knapCap = 10;
	const int size = 5;
	int weights[size] = { 1,1,1,1,1 };
	int values[size] = { 10,10,10,10,10 };
	REQUIRE(knapSackST(knapCap, size, weights, values).first == 50);
}

TEST_CASE("MultiT correct result -- Basic") {
	int knapCap = 10;
	const int size = 5;
	int weights[size] = { 1,2,3,4,5 };
	int values[size] = { 10,20,30,40,60 };
	REQUIRE(knapSackMT(knapCap, size, weights, values).first == 110);
}

TEST_CASE("MultiT correct result -- Same items") {
	int knapCap = 10;
	const int size = 5;
	int weights[size] = { 1,1,1,1,1 };
	int values[size] = { 10,10,10,10,10 };
	REQUIRE(knapSackMT(knapCap, size, weights, values).first == 50);
}

TEST_CASE("SingleT equals MultiT") {
	int knapCap = 50;
	const int size = 10;
	int values[size];
	int	weights[size];
	prepareArray(values, size);
	prepareArray(weights, size);
	REQUIRE(knapSackST(knapCap, size, weights, values) == knapSackMT(knapCap, size, weights, values));
}
